// JavaScript Document
var galleryItems = [
    {
		thumbImg: 'images/content/section-slideshow/thumbnail/Thumbnail1.jpg',
		fullImg: 'images/content/section-slideshow/fullsize/Galery1.jpg',
		hoverDescr: 'Lorem Ipsum Dolores',
    },
    {
        thumbImg: 'images/content/section-slideshow/thumbnail/Thumbnail2.jpg',
        fullImg: 'images/content/section-slideshow/fullsize/Galery2.jpg',
		hoverDescr: 'Lorem Ipsum Dolores',
    },
	{
        thumbImg: 'images/content/section-slideshow/thumbnail/Thumbnail3.jpg',
        fullImg: 'images/content/section-slideshow/fullsize/Galery3.jpg',
		hoverDescr: 'Lorem Ipsum Dolores',
    },
    {
        thumbImg: 'images/content/section-slideshow/thumbnail/Thumbnail4.jpg',
        fullImg: 'images/content/section-slideshow/fullsize/Galery4.jpg',
		hoverDescr: 'Lorem Ipsum Dolores',
    },
    {
        thumbImg: 'images/content/section-slideshow/thumbnail/Thumbnail5.jpg',
        fullImg: 'images/content/section-slideshow/fullsize/Galery5.jpg',
		hoverDescr: 'Lorem Ipsum Dolores',
    },
    {
        thumbImg: 'images/content/section-slideshow/thumbnail/Thumbnail6.jpg',
        fullImg: 'images/content/section-slideshow/fullsize/Galery6.jpg',
		hoverDescr: 'Lorem Ipsum Dolores',
    },
	{
        thumbImg: 'images/content/section-slideshow/thumbnail/Thumbnail7.jpg',
        fullImg: 'images/content/section-slideshow/fullsize/Galery7.jpg',
		hoverDescr: 'Lorem Ipsum Dolores',
    },
    {
        thumbImg: 'images/content/section-slideshow/thumbnail/Thumbnail8.jpg',
        fullImg: 'images/content/section-slideshow/fullsize/Galery8.jpg',
		hoverDescr: 'Lorem Ipsum Dolores',
    }
]

var ChartItems = [
    /* ThemeRed chartBackground-White */
	/*themeRed = [
		{
			chartData: [
                    ['17.138', 2000, '51%'],
                    ['13.338', 1500, '47%'],
                    ['30.568', 2750, '72%'],
                    ['17.778', 2000, '53%']
            ],
			sColors: [ "#a1a1a1", "#a1a1a1", "#ff4d4e", "#a1a1a1" ],
			dataText:
                    {
                        title: '45%',
                        subTitle: 'Lorem Ipsum',
                        description: 'Cаt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati',
                        projects:[
                            {
                                line1: '1300',
                                line2: 'New projects'
                            },
                            {
                                line1: '185',
                                line2: 'Followers'
                            },
                            {
                                line1: '57',
                                line2: 'Total project'
                            }
                        ]
                    }
		},
		{
			chartData: [
                    ['12.138', 250, 'Lorem'],
                    ['2.338', 2500, 'IPSUM'],
                    ['45.568', 1750, 'dolores'],
                    ['4.778', 500, 'SIT']
            ],
			sColors: [ "#a1a1a1", "#ff4d4e", "#a1a1a1", "#a1a1a1" ],
			dataText:
                    {
                        title: '45% faster',
                        subTitle: 'then previous',
                        description: 'Lorem Ipsum Dolores Sit amet adis',
                        projects:[
                            {
                                line1: '11300',
                                line2: 'Old projects'
                            },
                            {
                                line1: '0',
                                line2: 'Followers'
                            },
                            {
                                line1: 'Lorem Ipsum',
                                line2: 'Sit amet'
                            }
                        ]
                    }
		},
		{
			chartData: [
                    ['47.138', 2900, '21%'],
                    ['13.338', 17500, '87%'],
                    ['20.568', 2750, '72%'],
                    ['457.778', 1500, '13%']
            ],
			sColors: [ "#a1a1a1", "#ff4d4e", "#a1a1a1", "#a1a1a1" ],
			dataText:
                    {
                        title: 'IPSUM',
                        subTitle: 'molestias turi',
                        description: 'deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati Cаt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum',
                        projects:[
                            {
                                line1: 'deleniti',
                                line2: 'atque'
                            },
                            {
                                line1: 'corrupti',
                                line2: 'dolores'
                            },
                            {
                                line1: 'quos',
                                line2: 'molestias excepturi sint occaecati'
                            }
                        ]
                    }
		},
		{
			chartData: [
                    ['2.138', 20000, '100%'],
                    ['13.338', 1500, '15%'],
                    ['20.568', 2750, '12%'],
                    ['0.778', 2000, '17%']
            ],
			sColors: [ "#ff4d4e", "#a1a1a1", "#a1a1a1",  "#a1a1a1" ],
			dataText:
                    {
                        title: '7%',
                        subTitle: 'Thiner',
                        description: 'blanditiis praesentium voluptatum deleniti atque corrupti quos dol  blanditiis praesentium voluptatum deleniti atque corrupti quos dolCаt vero eos et accusamus et iusto odio dignissimos ducimus',
                        projects:[
                            {
                                line1: 'blanditiis',
                                line2: 'praesentium voluptatum deleniti'
                            },
                            {
                                line1: 'corrupti quos dol',
                                line2: 'landitiis praesentium'
                            },
                            {
                                line1: 'atque',
                                line2: 'Excepturi stias'
                            }
                        ]
                    }
		}
	],*/
    /* ThemeRed chartBackground-Red */
    themeRed = [
        {
            chartData: [
                ['17.138', 2000, '51%'],
                ['13.338', 1500, '47%'],
                ['30.568', 2750, '72%'],
                ['17.778', 2000, '53%']
            ],
            sColors: [ "#e77b7c", "#e77b7c", "#ffffff", "#e77b7c" ],
            dataText:
            {
                title: '45%',
                subTitle: 'faster then previous',
                description: 'Cаt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati',
                projects:[
                    {
                        line1: '1300',
                        line2: 'New projects'
                    },
                    {
                        line1: '185',
                        line2: 'Followers'
                    },
                    {
                        line1: '57',
                        line2: 'Total project'
                    }
                ]
            }
        },
        {
            chartData: [
                ['12.138', 250, 'Lorem'],
                ['2.338', 2500, 'IPSUM'],
                ['45.568', 1750, 'dolores'],
                ['4.778', 500, 'SIT']
            ],
            sColors: [ "#e77b7c", "#ffffff", "#e77b7c", "#e77b7c" ],
            dataText:
            {
                title: '45% faster',
                subTitle: 'then previous',
                description: 'Lorem Ipsum Dolores Sit amet adis',
                projects:[
                    {
                        line1: '11300',
                        line2: 'Old projects'
                    },
                    {
                        line1: '0',
                        line2: 'Followers'
                    },
                    {
                        line1: '123',
                        line2: 'Sit amet'
                    }
                ]
            }
        },
        {
            chartData: [
                ['47.138', 2900, '21%'],
                ['13.338', 17500, '87%'],
                ['20.568', 2750, '72%'],
                ['457.778', 1500, '13%']
            ],
            sColors: [ "#e77b7c", "#ffffff", "#e77b7c", "#e77b7c" ],
            dataText:
            {
                title: '13',
                subTitle: 'Winning Awards',
                description: 'deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati Cаt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum',
                projects:[
                    {
                        line1: 'deleniti',
                        line2: 'atque'
                    },
                    {
                        line1: 'corrupti',
                        line2: 'dolores'
                    },
                    {
                        line1: 'quos',
                        line2: 'molestias'
                    }
                ]
            }
        },
        {
            chartData: [
                ['2.138', 20000, '100%'],
                ['13.338', 1500, '15%'],
                ['20.568', 2750, '12%'],
                ['0.778', 2000, '17%']
            ],
            sColors: [ "#ffffff", "#e77b7c", "#e77b7c",  "#e77b7c" ],
            dataText:
            {
                title: '7%',
                subTitle: 'Lighter and thiner',
                description: 'blanditiis praesentium voluptatum deleniti atque corrupti quos dol  blanditiis praesentium voluptatum deleniti atque corrupti quos dolCаt vero eos et accusamus et iusto odio dignissimos ducimus',
                projects:[
                    {
                        line1: 'blanditiis',
                        line2: 'praesentium '
                    },
                    {
                        line1: 'corrupti',
                        line2: 'landitiis'
                    },
                    {
                        line1: 'atque',
                        line2: 'Excepturi stias'
                    }
                ]
            }
        }
    ],
	
	themeCmm = [
		{
			chartData: [
                    ['17.138', 2000, '51%'],
                    ['13.338', 1500, '47%'],
                    ['30.568', 2750, '72%'],
                    ['17.778', 2000, '53%']
            ],
			sColors: [ "#a1a1a1", "#a1a1a1", "#2993c6", "#a1a1a1" ],
			dataText:
                    {
                        title: '45%',
                        subTitle: 'faster then previous',
                        description: 'Cаt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati',
                        projects:[
                            {
                                line1: '1300',
                                line2: 'New projects'
                            },
                            {
                                line1: '185',
                                line2: 'Followers'
                            }
                        ]
                    }
		},
		{
			chartData: [
                    ['12-17', 13, '13%'],
                    ['18-24', 29, '29%'],
                    ['25-34', 27, '27%'],
                    ['35-34', 19, '19']
            ],
			sColors: [ "#a1a1a1", "#2993c6", "#a1a1a1", "#a1a1a1" ],
			dataText:
                    {
                        title: 'возраст',
                        subTitle: 'аудитории',
                        description: 'Большую часть активных пользавателей представляет возрастная группа 18-24 лет. Это студенты, шахтеры и садовники.',
                        projects:[
                            {
                                line1: '500000000',
                                line2: 'Зарегистрированых пользователей'
                            },
                            {
                                line1: '20000',
                                line2: 'Новых пользователей в день'
                            }
                        ]
                    }
		},
		{
			chartData: [
                    ['47.138', 2900, '21%'],
                    ['13.338', 17500, '87%'],
                    ['20.568', 2750, '72%'],
                    ['457.778', 1500, '13%']
            ],
			sColors: [ "#a1a1a1", "#2993c6", "#a1a1a1", "#a1a1a1" ],
			dataText:
                    {
                        title: 'IPSUM',
                        subTitle: 'molestias turi',
                        description: 'deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati Cаt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum',
                        projects:[
                            {
                                line1: 'deleniti',
                                line2: 'atque'
                            },
                            {
                                line1: 'corrupti',
                                line2: 'dolores'
                            }
                        ]
                    }
		},
		{
			chartData: [
                    ['Мужчины', 200, '5%'],
                    ['Женщины', 1500, '10%'],
                    ['Олени', 27500, '85%'],
            ],
			sColors: [ "#a1a1a1", "#a1a1a1", "#2993c6" ],
			dataText:
                    {
                        title: '7%',
                        subTitle: 'Thiner',
                        description: 'blanditiis praesentium voluptatum deleniti atque corrupti quos dol  blanditiis praesentium voluptatum deleniti atque corrupti quos dolCаt vero eos et accusamus et iusto odio dignissimos ducimus',
                        projects:[
                            {
                                line1: 'blanditiis',
                                line2: 'praesentium voluptatum deleniti'
                            },
                            {
                                line1: 'corrupti quos dol',
                                line2: 'landitiis praesentium'
                            }
                        ]
                    }
		}
	]
	
]


