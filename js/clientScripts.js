$(window).load(function() {

    clientScripts = {
        init: function () {
            this.initEvents();
        },

        initEvents: function () {

			$('.demo-video').click($.proxy(this.demoVideo, this));

            function isTouchDevice(){
                return (typeof(window.ontouchstart) != 'undefined') ? true : false;
            };

            if (isTouchDevice() == false ) {
                if (theme != 'cmm') {
                    $(document).ready($.proxy(this.RightNav, this));
                }
                $(document).ready($.proxy(this.AnimationLoad, this));
            }
            else {
                $('body').addClass('touchDevice');
                $(document).ready($.proxy(this.charts, this));
            }
			
			$('.right-nav a, .headwrap .marker').click($.proxy(this.ScrollPage, this));
			
            $(document).ready($.proxy(this.Slider1, this));
            $(document).ready($.proxy(this.Slider2, this));

            $('.img-for-chart .marker').click($.proxy(this.selectChart, this));

            if ($('.section-slideshow').length) {
                $(document).ready($.proxy(this.GalleryLoad, this));
                $('.sorting-nav a').click($.proxy(this.sortGallery, this));
                $(document).ready($.proxy(this.SlideShow, this));
            }

            if ($('.section-mailing').length) {
                $(document).ready($.proxy(this.mailingForm, this));
            }

            if ($('.section-cases').length) {
                $(document).ready($.proxy(this.casesForm, this));
            }

            $('.fancybox.send').click($.proxy(this.popupSend, this));


        },

		demoVideo: function (event) {
			event.preventDefault();

                $(event.currentTarget).fancybox({
                    'padding'       : 0,
                    'autoScale'     : true,
                    tpl: {
                        closeBtn: '<a title="Close" class="fancybox-item fancybox-close demo-video-close" href="javascript:;"></a>'
                    },
                    /*'href'          : $(event.currentTarget).attr('href').replace(new RegExp("watch\\?v=", "i"), 'v/'),
                    'type'          : 'swf',
                    'swf'           : {
                        'wmode'             : 'transparent',
                        'allowfullscreen'   : 'true'
                    }*/
                });


            return false;
		},

        popupSend: function (event) {
            event.preventDefault();

            $.fancybox({
                'padding'       : 0,
                type: 'iframe',
                width: 600,
                height: 278,
                autoSize : false,
                'autoScale'     : false,
                'content'       : $('#fancySendForm'),
            });

            return false;
        },

        RightNav: function () {

            function navDisplay() {
                var headwrapPositionBottom = $('.headwrap').height();

                var rightNavPosition = $('.right-nav').offset().top;
                if (rightNavPosition > headwrapPositionBottom )
                {
                    $('.right-nav').fadeIn();
                }
                else
                {
                    $('.right-nav').fadeOut();
                }

                if ($('div.row.services').length) {
                    var settingsPosition = $('div.row.services').offset().top;
                    var settingsPositionBottom = $('div.row.services').height()+settingsPosition;

                    if ( (rightNavPosition > settingsPosition) &&  (rightNavPosition < settingsPositionBottom)) {
                        $('.right-nav a').removeClass('current');
                        $('.right-nav a.settings').addClass('current');
                    }
                }

                if ($('div.section-camera').length) {
                    var cameraPosition = $('div.section-camera').offset().top;
                    var cameraPositionBottom = $('div.section-camera').height()+cameraPosition;

                    if ( (rightNavPosition > cameraPosition) &&  (rightNavPosition < cameraPositionBottom)) {
                        $('.right-nav a').removeClass('current');
                        $('.right-nav a.camera').addClass('current')
                    }
                }

                if ($('div.section-chart').length) {
                    var chartPosition = $('div.section-chart').offset().top;
                    var chartPositionBottom = $('div.section-chart').height()+chartPosition;

                    if ( (rightNavPosition > chartPosition) &&  (rightNavPosition < chartPositionBottom)) {
                        $('.right-nav a').removeClass('current');
                        $('.right-nav a.chart').addClass('current');
                    }
                }

                if ($('div.section-slideshow').length) {
                    var slideshowPosition = $('div.section-slideshow').offset().top;
                    var slideshowPositionBottom = $('div.section-slideshow').height()+slideshowPosition;

                    if (( rightNavPosition > slideshowPosition) &&  (rightNavPosition < slideshowPositionBottom)) {
                        $('.right-nav a').removeClass('current');
                        $('.right-nav a.slideshow').addClass('current')
                    }
                }

                if ($('div.section-mailing').length) {
                    var mailingPosition = $('div.section-mailing').offset().top;
                    var mailingPositionBottom = $('div.section-mailing').height()+mailingPosition;

                    if ( rightNavPosition > mailingPosition) {
                        $('.right-nav a').removeClass('current');
                        $('.right-nav a.mailing').addClass('current')
                    }
                }

            }
			
			$(window).on('scroll', function () {
			    navDisplay();
			})
			navDisplay();

            

        },

        ScrollPage: function (event) {
			event.preventDefault();
            var AnchorName = $(event.currentTarget).attr('href');
            var TargetModule = $(AnchorName);
            $('html,body').animate({scrollTop: TargetModule.offset().top}, 500);

        },
		
		AnimationLoad: function () {
			function startAnim () {
				var windowHeight = $(window).height();
				var currScroll =  window.pageYOffset;

                if (!($('.headwrap').hasClass('animate'))) {
                    var headPosition = 0;
                    var headHeight = $('div.headwrap').height()

                    if ( ((currScroll+windowHeight-100) > headPosition) && ((currScroll) < (headHeight)) ) {
                        $('.headwrap').addClass('animate')
                    }
                }

				if (!($('.services').hasClass('animate'))) {
					var servicesPosition = $('div.row.services').offset().top;
					var servicesHeight = $('div.row.services').height()
					
					if ( ((currScroll+windowHeight-100) > servicesPosition) && ((currScroll) < (servicesPosition+servicesHeight)) ) {
						$('.services').addClass('animate')
					}
				}
				
				
				if (!($('#settings .carousel').hasClass('animate'))) {
					var settingsPosition = $('#settings').offset().top;
					var settingsHeight = $('#settings').height()
					
					if ( ((currScroll+windowHeight) > settingsPosition) && ((currScroll) < (settingsPosition+settingsHeight)) ) {
						$('#settings .carousel').addClass('animate')
					}
				}
				
				if (!($('.module-chart').hasClass('animate'))) {
					var chartPosition = $('.module-chart').offset().top;
					var chartHeight = $('.module-chart').height()
					
					if ( ((currScroll+windowHeight-500) > chartPosition) && ((currScroll) < (chartPosition+chartHeight-100)) ) {
						$('.module-chart').addClass('animate');
						clientScripts.charts()
					}
				}
				
				
			}
			startAnim();
			 $(window).on('scroll', function () {
                startAnim();
            })
		},

        Slider1: function () {
            $('#slider1').carousel({
                interval: false
            })
        },

        Slider2: function () {

            $('#slider2').carousel({
                interval: 5000,
                pause: "false"
            })

            $('#slider2').on('slide.bs.carousel',function(){
                $('.full-screen.carousel .active .text-box').fadeOut();
            });

            $('#slider2').on('slid.bs.carousel',function(){
                $('.full-screen.carousel .active .text-box').fadeIn();
            });

        },

        charts: function () {
            var selectedChart = $('.img-for-chart .marker.selected').attr('id');

            $.getScript( "js/DataItems.js" )
                .done(function( ChartItems, data ) {

                    if (theme == 'cmm') {
                        chartDataType = themeCmm;
                    }
                    else {
                        chartDataType = themeRed;
                    }


            if ( selectedChart == 'chart1') {
                var chartData = chartDataType[1].chartData;
                var sColors = chartDataType[1].sColors;
                var dataText = chartDataType[1].dataText

                chartDraw(chartData, sColors, dataText)
            }
            else if ( selectedChart == 'chart2') {
                var chartData = chartDataType[2].chartData;
                var sColors = chartDataType[2].sColors;
                var dataText = chartDataType[2].dataText;

                chartDraw(chartData, sColors, dataText)
            }
            else if ( selectedChart == 'chart3') {
                var chartData = chartDataType[3].chartData;
                var sColors = chartDataType[3].sColors;
                var dataText = chartDataType[3].dataText;

                chartDraw(chartData, sColors, dataText)
            }
            else if ( selectedChart == 'chart4') {
                var chartData = chartDataType[3].chartData;
                var sColors = chartDataType[3].sColors;
                var dataText = chartDataType[3].dataText;

                chartDraw(chartData, sColors, dataText)
            }

            function chartDraw (chartData, sColors, dataText) {
                $.jqplot.config.enablePlugins = true;

                if (theme == 'cmm') {
                    chartDataType = themeCmm;
                    var gridColor = '#2993c6';
                    var yAxisShow = false;
                }
                else {
                    chartDataType = themeRed;
                    var gridColor = '#ffffff';
                    var yAxisShow = true;
                }

                plot = $.jqplot('chart-container', [chartData], {
                    // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                    animate: !$.jqplot.use_excanvas,
                    animateReplot: true,
                    seriesDefaults: {
                        renderer: $.jqplot.BarRenderer,
                        rendererOptions: { varyBarColor: true },
                        pointLabels: { show: true },
                        shadow: false,
                        trendline: {
                            show: false,
                            color: 'transparent',
                        }
                    },
                    seriesColors: $.merge(sColors, $.jqplot.config.defaultColors),
                    grid: {
                        background: 'transparent',
                        drawGridLines: false,
                        shadow: false,
                        borderWidth: 0.0,
                        borderColor: 'transparent'
                    },
                    axes: {
                        xaxis: {
                            renderer: $.jqplot.CategoryAxisRenderer,
                            tickOptions: {
                                showGridline: false,
                                showMark: false,
                                fontFamily: 'Lato Regular',
                                textColor: gridColor,
                                fontSize: '12px'
                            }
                        },
                        yaxis: {
//                        renderer: $.jqplot.CategoryAxisRenderer,
                            tickOptions: {
                                show: yAxisShow,
                                showGridline: false,
                                fontFamily: 'Lato Regular',
                                textColor: gridColor,
                                fontSize: '12px',
                            }
                        }
                    },
                    highlighter: { show: false }
                });

                plot.replot();
                $('.box-chart .title').html(dataText.title);
                $('.box-chart .sub-title').html(dataText.subTitle)
                $('.box-chart .chart-text').html(dataText.description);
                $('.box-chart .row.projects .text-center').each(function (i) {
                    $(this).find('h3').html(dataText.projects[i].line1);
                    $(this).find('p').html(dataText.projects[i].line2);
                })
            }
			/*$('#chart-container').bind('jqplotDataClick',
				function (ev, seriesIndex, pointIndex, data) {
					$('.chart-text').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
				}
			);*/
                })
        },

        selectChart: function (event) {
            $('.img-for-chart .marker').removeClass('selected');
            $(event.currentTarget).addClass('selected');
            clientScripts.charts();
        },
		
		GalleryLoad: function () {
			$.getScript( "js/DataItems.js" )
			  .done(function( galleryItems, data ) {
				addGalleryItems ( galleryItems );

			  })
			  .fail(function() {
				alert('Failed load json')
			});
			
			function addGalleryItems () {
				var i = 0;
				while(i < 8){ 
					(function(i) {
                        var retina = window.devicePixelRatio > 1;
                        if (retina) {
                            var thumbImage = galleryItems[i].thumbImg.replace(/\.(?=[^.]*$)/, "@x2.");
                            var fullImage = galleryItems[i].fullImg.replace(/\.(?=[^.]*$)/, "@x2.");
                        }
                        else {
                            var thumbImage = galleryItems[i].thumbImg;
                            var fullImage = galleryItems[i].fullImg;
                        }

						$('.gallery-items .row').append(
			
							'<div class="col-xs-3 item">'+
							'     <div class="thumbnail">'+
							'        <img src="'+thumbImage+'" />'+
							'       <a class="mask fancybox" href="'+fullImage+'" rel="group">'+
							'            <span class="icon-expand"></span>'+
							'        	<p class="description">'+galleryItems[i].hoverDescr+'</p>'+
							'    	</a>'+
							' 	</div>'+
							'</div>'
								
						)	
								
										
					})(i++);	
				}
                var maxItems = galleryItems.length;
                if ((maxItems == 8) || (maxItems < 8)) {
                    $('.button-load-more').hide();
                }
				$('.button-load-more').click($.proxy(clientScripts.LoadMore, this))	
			}
			
		},

        sortGallery: function (event) {

            if (!event) {
                var filterSelected =  $('.sorting-nav a.selected')
            }
            else {
                var filterSelected =  $(event.currentTarget);
            }

        //    if (!(filterSelected.hasClass('selected'))) {
                $('.sorting-nav a').removeClass('selected');
                var selectedOption = filterSelected.attr('class');
                filterSelected.addClass('selected');
                if (selectedOption == 'medium-size') {
                    $('.gallery-items .item').each(function () {
                        $(this).animate({ width: '33.3%'}, 500).fadeIn();
                    })
                }
                else if (selectedOption == 'small-size') {
                    $('.gallery-items .item').each(function () {
                        $(this).animate({ width: '25%'}, 500).fadeIn();
                    })
                }
				else if (selectedOption == 'big-size') {
					var j = 0;
					var k = 0;
					$('.gallery-items .item').each(function (i) {
                       if ((i+1) == (3*j + 1)) {
							$(this).animate({ width: '50%'}, 500).fadeIn();
							
							if ((i + 1) == (6*k + 4)) {
								$(this).css('float','right');
								k++
					   		}
							
							j++   
					   }
					   else
					   {
						    $(this).animate({ width: '25%'}, 500).fadeIn();
					   }  
                    })
				}
        //    }
        },
		
		SlideShow: function () {
			$(".slideshow-container .fancybox").fancybox();
		},
		
		LoadMore: function () {
			//galleryItems.length
			
			var numberItems = $('.gallery-items .item').length;
			var maxItems = galleryItems.length;
			var i = numberItems - 1;
			var j = numberItems + 3;
			while(i < j) {
				 
					(function(){
                        var retina = window.devicePixelRatio > 1;
                        if (retina) {
                            var thumbImage = galleryItems[i].thumbImg.replace(/\.(?=[^.]*$)/, "@x2.");
                            var fullImage = galleryItems[i].fullImg.replace(/\.(?=[^.]*$)/, "@x2.");
                        }
                        else {
                            var thumbImage = galleryItems[i].thumbImg;
                            var fullImage = galleryItems[i].fullImg;
                        }
                        $(
                                '<div class="col-xs-3 item" style=" display: none;">'+
                                '     <div class="thumbnail">'+
                                '       <img src="'+thumbImage+'" />'+
                                '       <a class="mask fancybox" href="'+fullImage+'" rel="group">'+
                                '           <span class="icon-expand"></span>'+
                                '        	<p class="description">'+galleryItems[i].hoverDescr+'</p>'+
                                '    	</a>'+
                                ' 	</div>'+
                                '</div>'

                        ).appendTo('.gallery-items .row')


										
				})(i++);
				if (maxItems == (i+1)) {
					$('.button-load-more').fadeOut();
					break;
				}
				
			}
			
			if ($(window).width() > 767 ) {
            	clientScripts.sortGallery();
			}
			
		},

        mailingForm: function () {



        },

        casesForm: function () {
            $('.section-cases .case-type').on('click', function () {
                $('.section-cases .case-type').removeClass('selected');
                $(this).addClass('selected');
                $(this).siblings('input').click();
            })
        }
		
		
    };


    var retina = window.devicePixelRatio > 1;

    if (retina) {
        function modForRetina () {
            var lowresImages = $('img');

            lowresImages.each(function(i) {
                var lowres = $(this).attr('src');
                var highres = lowres.replace(/\.(?=[^.]*$)/, "@x2.");
                $(this).attr('src', highres);
            });

            $('.section-camera').find('.fill').each(function() {
                var lowres_img = $(this).css('background-image');
                var highres_img = lowres_img.replace(/\.(?=[^.]*$)/, "@x2.");
                $(this).css('background-image', highres_img);
				
            })

            $('.section-settings').find('.img-container').each(function() {
                var lowres_img = $(this).css('background-image');
                var highres_img = lowres_img.replace(/\.(?=[^.]*$)/, "@x2.");
                $(this).css('background-image', highres_img);
            })

            $('.load-indicator').fadeOut('slow')
        }
        $.when(
            modForRetina ()
        ).then(function() {

            clientScripts.init();
            $('.page-wrapper').fadeIn('slow');
        });
    }
    else {
        $.when(
            $('.load-indicator').fadeOut('slow')
        ).then(function() {
            clientScripts.init();
            $('.page-wrapper').fadeIn('slow');
        });
    }





})